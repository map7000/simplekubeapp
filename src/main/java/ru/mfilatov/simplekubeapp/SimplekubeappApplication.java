package ru.mfilatov.simplekubeapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimplekubeappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimplekubeappApplication.class, args);
	}

}
